package com.ents.networks.server;

import com.ents.networks.util.ChecksumManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by APS on 21-11-2016.
 */
@Configuration
@PropertySource("file:./resources/server/server.properties")
public class ServerSpringConfig {
    @Value("${server.datagramSocket.port:6992}")
    int serverDatagramConnectionPort;

    @Value("${server.measurement.dataFile:./resources/server/data.txt}")
    String measurementsDataFile;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    Server getServer() throws IOException {
        return new Server(serverDatagramConnectionPort,getServerMessageManager());
    }

    @Bean
    public ServerMessageManager getServerMessageManager() throws IOException {
        return new ServerMessageManager(getChecksumManager(),getMeasurementDataLoader());
    }
    @Bean
    public ChecksumManager getChecksumManager() {
        return new ChecksumManager();
    }

    @Bean
    public MeasurementDataLoader getMeasurementDataLoader() throws IOException {
        return new MeasurementDataLoader(new File(measurementsDataFile));
    }
}
