package com.ents.networks.server;

import java.io.IOException;
import java.net.*;

/**
 * Created by APS on 20-11-2016.
 */
public class Server {
    private final DatagramSocket SERVER_SOCKET_CONNECTION;
    private final int PORT;
    private final ServerMessageManager SERVER_MSG_MANAGER;
    private SocketAddress clientSocketAddress;

    public Server(int PORT, ServerMessageManager serverMessageManager) throws SocketException {
        this.PORT = PORT;
        SERVER_MSG_MANAGER = serverMessageManager;
        SERVER_SOCKET_CONNECTION = new DatagramSocket(this.PORT);
    }

    public void init() throws IOException {
        System.out.println("Server Started successfully on port: "+PORT);
        System.out.println("Waiting for clients to connect...");


        while(true)
        {
            byte[] receiveData = new byte[1024];
            String requestMsg = receiveRequest(receiveData);
            sendResponse(requestMsg);
        }
    }

    private String receiveRequest(byte[] receiveData) throws IOException {
        DatagramPacket requestPacket = new DatagramPacket(receiveData, receiveData.length);
        SERVER_SOCKET_CONNECTION.receive(requestPacket);
        String requestMsg = new String(requestPacket.getData());
        final InetAddress clientInetAddressObj = requestPacket.getAddress();
        int clientPort = requestPacket.getPort();
        clientSocketAddress = new InetSocketAddress(clientInetAddressObj,clientPort);
        return requestMsg;
    }

    private String generateResponseMsg(String requestMsg) {
        requestMsg = requestMsg.replaceAll("\\s|\n|\t|\u0000","");
        if (SERVER_MSG_MANAGER.validateRequestMsg(requestMsg)){
            SERVER_MSG_MANAGER.parseRequestMsg(requestMsg);
        }
        return SERVER_MSG_MANAGER.generateResponse();
    }

    private void sendResponse(String requestMsg) throws IOException {
        String responseMsg = generateResponseMsg(requestMsg);
        byte [] sendData=responseMsg.getBytes();
        DatagramPacket responsePacket = new DatagramPacket(sendData, sendData.length, clientSocketAddress);
        SERVER_SOCKET_CONNECTION.send(responsePacket);;

    }

    public void destroy(){
        SERVER_SOCKET_CONNECTION.close();
    }
}
