package com.ents.networks.server;

/**
 * Created by APS on 20-11-2016.
 */
public class ResponseObject {
    private int responseId;
    private int statusCode;
    private int measurementId;
    private Float measurementValue;

    public void setResponseId(int responseId) {
        this.responseId = responseId;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setMeasurementId(int measurementId) {
        this.measurementId = measurementId;
    }

    public void setMeasurementValue(Float measurementValue) {
        this.measurementValue = measurementValue;
    }

    public int getResponseId() {
        return responseId;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getMeasurementId() {
        return measurementId;
    }

    public Float getMeasurementValue() {
        return measurementValue;
    }
}
