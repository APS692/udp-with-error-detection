package com.ents.networks.server;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by APS on 20-11-2016.
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationContext = new AnnotationConfigApplicationContext(ServerSpringConfig.class);
        annotationContext.registerShutdownHook();
    }

}
