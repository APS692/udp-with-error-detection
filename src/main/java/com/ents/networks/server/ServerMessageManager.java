package com.ents.networks.server;

import com.ents.networks.util.ChecksumManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by APS on 20-11-2016.
 */
public class ServerMessageManager {

    private static final Pattern REQUEST_CHECKSUM_PATTERN = Pattern.compile("</request>(\\d+)");
    private static final Pattern REQUEST_ID_PATTERN = Pattern.compile("<id>(\\d+)</id>");
    private static final Pattern REQUEST_MEASUREMENT_ID_PATTERN = Pattern.compile("<measurement>(\\d+)</measurement>");
    private static final Pattern REQUEST_MSG_PATTERN = Pattern.compile(String.format("<request>%s%s</request>(\\d+)", REQUEST_ID_PATTERN.toString(), REQUEST_MEASUREMENT_ID_PATTERN.toString()));
    private static final String RESPONSE_MESSAGE_FORMAT = "<response><id>%d</id><code>%d</code><measurement>%d</measurement><value>%s</value></response>";
    private static final String RESPONSE_ERROR_MESSAGE_FORMAT = "<response><id>%d</id><code>%d</code></response>";
    private static final String FINAL_RESPONSE_MESSAGE_PATTERN = "%s%s";
    private final ChecksumManager SERVER_CHECKSUM_MANAGER;
    private final MeasurementDataLoader DATA_LOADER;
    private ResponseObject responseObject;

    public ServerMessageManager(ChecksumManager server_checksum_manager, MeasurementDataLoader data_loader) {
        SERVER_CHECKSUM_MANAGER = server_checksum_manager;
        DATA_LOADER = data_loader;
    }


    boolean validateRequestMsg(String requestMsg) {
        responseObject = new ResponseObject();
        boolean syntaxErrorFlag = true, checksumErrorFlag = true;
        try {
            String checksumReceived = getMatchedPattern(REQUEST_CHECKSUM_PATTERN, requestMsg, 1);
            if (checksumReceived != null) {
                String requestMsgWithoutChecksum = requestMsg.replace(checksumReceived, "");
                checksumErrorFlag = SERVER_CHECKSUM_MANAGER.checksumValidate(requestMsgWithoutChecksum, checksumReceived);
            }
            if (checksumErrorFlag) {
                syntaxErrorFlag = (getMatchedPattern(REQUEST_MSG_PATTERN, requestMsg) == null) ? false : true;
                if(!syntaxErrorFlag){
                    responseObject.setStatusCode(2);
                }
            } else {
                responseObject.setStatusCode(1);
            }

        } catch (Exception e) {
            syntaxErrorFlag = false;
            e.printStackTrace();
        }
        return syntaxErrorFlag && checksumErrorFlag;
    }

    private String getMatchedPattern(Pattern msgPattern, String msg, Integer... args) {
        try {
            Matcher patternMatcher = msgPattern.matcher(msg);
            patternMatcher.find();
            if (args.length == 0) {
                return patternMatcher.group();
            }
            return patternMatcher.group(args[0]);
        } catch (Exception e) {

        }
        return null;
    }

    public ResponseObject getResponseObject() {
        return responseObject;
    }

    void parseRequestMsg(String requestMsg) {


        Integer requestId = Integer.parseInt(getMatchedPattern(REQUEST_ID_PATTERN, requestMsg, 1));
        Integer measureId = Integer.parseInt(getMatchedPattern(REQUEST_MEASUREMENT_ID_PATTERN, requestMsg, 1));

        if (requestId != null) {
            responseObject.setResponseId(requestId);
        }
        if (measureId != null) {
            responseObject.setMeasurementId(measureId);
        }


    }

    public String generateResponse() {
        if (responseObject.getStatusCode() == 0) {
            Float temperatureValue = DATA_LOADER.getTemperature(responseObject.getMeasurementId());
            if (temperatureValue != null) {
                responseObject.setStatusCode(0);
                responseObject.setMeasurementValue(temperatureValue);
            } else {
                responseObject.setStatusCode(3);
            }
        }

        String responseMsg = "";
        if (responseObject.getStatusCode() == 0) {
            responseMsg = String.format(RESPONSE_MESSAGE_FORMAT, responseObject.getResponseId(), responseObject.getStatusCode(), responseObject.getMeasurementId(), responseObject.getMeasurementValue());
        } else {
            responseMsg = String.format(RESPONSE_ERROR_MESSAGE_FORMAT, responseObject.getResponseId(), responseObject.getStatusCode());
        }
        return String.format(FINAL_RESPONSE_MESSAGE_PATTERN, responseMsg, SERVER_CHECKSUM_MANAGER.generateChecksum(responseMsg));
    }
}
