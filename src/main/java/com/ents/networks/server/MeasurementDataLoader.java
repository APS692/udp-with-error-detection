package com.ents.networks.server;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by APS on 21-11-2016.
 */
public class MeasurementDataLoader {
    private final Map<Integer,Float> TEMPERATURE_MAP = new HashMap<Integer,Float>();
    private final File TEMPERATURE_DATA_FILE;

    public MeasurementDataLoader(File temperatureDataFile) throws IOException {
        TEMPERATURE_DATA_FILE = temperatureDataFile;
        populateTemperatureMap();
    }

    private void populateTemperatureMap() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(TEMPERATURE_DATA_FILE));
        String line ="";

        while ((line = bufferedReader.readLine()) !=null){
            try{
                String[] lineArray = line.split("\\s|\t");
                Integer measureId = Integer.valueOf(lineArray[0]);
                Float temperatureValue = BigDecimal.valueOf(Float.valueOf(lineArray[1])).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
                TEMPERATURE_MAP.put(measureId,temperatureValue);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        bufferedReader.close();
    }

    public Float getTemperature(int measurementId) {
        return TEMPERATURE_MAP.get(measurementId);
    }
}
