package com.ents.networks.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by APS on 20-11-2016.
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationContext = new AnnotationConfigApplicationContext(ClientSpringConfig.class);
        annotationContext.registerShutdownHook();
    }


}
