package com.ents.networks.client;

/**
 * Created by APS on 20-11-2016.
 */
public enum StatusCodes {


    OK(0,"Response OK"),
    INTEGRITY_CHECK_FAILURE(1,"Integrity check failure.The request had one or more bit errors"),
    MALFORMED_REQUEST(2,"Malformed request.The syntax of the request message was not correct"),
    NON_EXISTENT_MEASUREMENT(3,"Non-existent measurement.The measurement with the requested measurement ID does not exist");


    private final int statusCode;
    private final String statusMsg;

    StatusCodes(int statusCode, String statusMsg){
        this. statusCode = statusCode;
        this.statusMsg = statusMsg;
    }

    public String getErrorMsg(){
        return statusMsg;
    }

    public int getErrorCode() {
        return  statusCode;
    }
}
