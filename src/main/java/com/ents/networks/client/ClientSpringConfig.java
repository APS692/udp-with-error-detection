package com.ents.networks.client;

import com.ents.networks.util.ChecksumManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by APS on 21-11-2016.
 */
@Configuration
@PropertySource("file:./resources/client/client.properties")
public class ClientSpringConfig {

    @Value("${server.datagramSocket.port:6992}")
    int serverDatagramConnectionPort;

    @Value("${server.ipAddress:127.0.0.1}")
    String serverIPAddress;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    Client getClient() throws SocketException, UnknownHostException {
        return new Client(serverIPAddress,serverDatagramConnectionPort,getClientMessageManager());
    }

    @Bean
    public ClientMessageManager getClientMessageManager() {
        return new ClientMessageManager(getChecksumManager());
    }

    @Bean
    public ChecksumManager getChecksumManager() {
        return new ChecksumManager();
    }
}
