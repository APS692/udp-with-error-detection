package com.ents.networks.client;

import com.ents.networks.server.ResponseObject;
import com.ents.networks.util.ChecksumManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by APS on 20-11-2016.
 */
public class ClientMessageManager {
    private static final Pattern RESPONSE_CHECKSUM_PATTERN = Pattern.compile("</response>(\\d+)");
    private static final Pattern RESPONSE_ID_PATTERN = Pattern.compile("<id>(\\d+)</id>");
    private static final Pattern RESPONSE_CODE_PATTERN = Pattern.compile("<code>(\\d+)</code>");
    private static final Pattern RESPONSE_MEASUREMENT_ID_PATTERN = Pattern.compile("<measurement>(\\d+)</measurement>");
    private static final Pattern RESPONSE_VALUE_PATTERN = Pattern.compile("<value>(\\d+(\\.\\d{1,2}))</value>");
    private static final Pattern RESPONSE_MSG_PATTERN = Pattern.compile(String.format("<response>%s%s%s%s</response>(\\d+)",
            RESPONSE_ID_PATTERN.toString(),
            RESPONSE_CODE_PATTERN.toString(),
            RESPONSE_MEASUREMENT_ID_PATTERN.toString(),
            RESPONSE_VALUE_PATTERN.toString()));
    private static final Pattern RESPONSE_ERROR_PATTERN = Pattern.compile(String.format("<response>%s%s</response>(\\d+)",
            RESPONSE_ID_PATTERN.toString(),
            RESPONSE_CODE_PATTERN.toString()));
    private static final String RESPONSE_MESSAGE_FORMAT = "<response><id>%d</id><code>%d</code><measurement>%d</measurement><value>%s</value></request>";
    private static final String RESPONSE_ERROR_MESSAGE_FORMAT = "<response><id>%d</id><code>%d</code></response></response>";

    private static final String REQUEST_MESSAGE_PATTERN = "<request><id>%d</id><measurement>%d</measurement></request>";
    private static final String FINAL_REQUEST_MESSAGE_PATTERN = "%s%s";
    private final ChecksumManager CLIENT_CHECKSUM_MANAGER;

    public ClientMessageManager(ChecksumManager checksumManager) {
        this.CLIENT_CHECKSUM_MANAGER = checksumManager;
    }

    String getFormattedRequestMsg(int requestId, int measurementId) {
        String formattedRequestMsg = String.format(REQUEST_MESSAGE_PATTERN, requestId, measurementId);
        String checksum = CLIENT_CHECKSUM_MANAGER.generateChecksum(formattedRequestMsg);
        return String.format(FINAL_REQUEST_MESSAGE_PATTERN, formattedRequestMsg, checksum);
    }

    boolean validateResponseMsg(String responseMsg) {
        boolean syntaxErrorFlag = true, checksumErrorFlag = true;
        try {
            String checksumReceived = getMatchedPattern(RESPONSE_CHECKSUM_PATTERN, responseMsg, 1);
            if (checksumReceived != null) {
                String responseMsgWithoutChecksum = responseMsg.replace(checksumReceived, "");
                checksumErrorFlag = CLIENT_CHECKSUM_MANAGER.checksumValidate(responseMsgWithoutChecksum, checksumReceived);
            }
            if (checksumErrorFlag) {
                syntaxErrorFlag = (getMatchedPattern(RESPONSE_MSG_PATTERN, responseMsg) == null
                        && (getMatchedPattern(RESPONSE_ERROR_PATTERN, responseMsg) == null)) ? false : true;
                if (syntaxErrorFlag) {
                    // Syntax of response not correct
                }
            } else {
                // Checksum of response not correct
            }

        } catch (Exception e) {
            syntaxErrorFlag = false;
            e.printStackTrace();
        }
        return syntaxErrorFlag && checksumErrorFlag;
    }

    public Float parseResponseMsgForMeasurement(String responseMsg) {
        Integer requestId = Integer.parseInt(getMatchedPattern(RESPONSE_ID_PATTERN, responseMsg, 1));
        Integer measureId = Integer.parseInt(getMatchedPattern(RESPONSE_MEASUREMENT_ID_PATTERN, responseMsg, 1));
        Float value = Float.parseFloat(getMatchedPattern(RESPONSE_VALUE_PATTERN, responseMsg, 1));
        return value;
    }

    public int parseResponseMsgForCode(String responseMsg) {
        return Integer.parseInt(getMatchedPattern(RESPONSE_CODE_PATTERN, responseMsg, 1));
    }

    private String getMatchedPattern(Pattern msgPattern, String msg, Integer... args) {
        try {
            Matcher patternMatcher = msgPattern.matcher(msg);
            patternMatcher.find();
            if (args.length == 0) {
                return patternMatcher.group();
            }
            return patternMatcher.group(args[0]);
        } catch (Exception e) {

        }
        return null;
    }

}
