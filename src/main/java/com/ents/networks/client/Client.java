package com.ents.networks.client;

import com.ents.networks.util.ChecksumManager;

import java.io.IOException;
import java.net.*;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by APS on 20-11-2016.
 */
public class Client {
    private final DatagramSocket CLIENT_SOCKET_CONNECTION;
    private final InetAddress SERVER_INET_ADDRESS_OBJ;
    private final SocketAddress SERVER_SOCKET_ADDRESS;
    private final ClientMessageManager CLIENT_MSG_MANAGER;
    private final int MAX_RETRIES = 4;

    public Client(String serverIpAddress, int serverPort, ClientMessageManager clientMessageManager) throws SocketException, UnknownHostException {

        SERVER_INET_ADDRESS_OBJ = InetAddress.getByName(serverIpAddress);
        CLIENT_SOCKET_CONNECTION = new DatagramSocket();
        SERVER_SOCKET_ADDRESS = new InetSocketAddress(SERVER_INET_ADDRESS_OBJ, serverPort);
        CLIENT_MSG_MANAGER = clientMessageManager;
    }

    public void init() throws IOException {
        System.out.println("Client Started successfully");
        while (true) {
            byte[] receiveData = new byte[1024];
            int measureId = takeMeasureIdInput();
            int requestId = new Random().nextInt((int) Math.pow(2, 16));
            int noOfTries = 1;
            int timeoutSecs = 1;

            while (noOfTries <= MAX_RETRIES) {
                try {
                    System.out.println("Sending request...Timeout: " + timeoutSecs + "s\n");
                    sendRequest(requestId, measureId);
                    String responseMsg = receiveResponse(receiveData, timeoutSecs * 1000);
                    responseMsg = responseMsg.replaceAll("\\s|\n|\t|\u0000", "");
                    if (responseMsg != null) {
                        // System.out.println("Response" + responseMsg);
                        if (CLIENT_MSG_MANAGER.validateResponseMsg(responseMsg)) {
                            int code = CLIENT_MSG_MANAGER.parseResponseMsgForCode(responseMsg);
                            if (code == StatusCodes.OK.getErrorCode()) {
                                System.out.println("Value" + CLIENT_MSG_MANAGER.parseResponseMsgForMeasurement(responseMsg));
                            } else if (code == StatusCodes.INTEGRITY_CHECK_FAILURE.getErrorCode()) {
                                System.out.println(StatusCodes.INTEGRITY_CHECK_FAILURE.getErrorMsg());
                                System.out.println("Do you want to try to send again?(y/n)");
                                Scanner sc = new Scanner(System.in);
                                char choice = sc.next().charAt(0);
                                switch (choice) {
                                    case 'y':
                                    case 'Y':
                                        break;
                                    default:
                                        System.out.println("Exiting...");
                                        System.exit(1);
                                }
                            } else if (code == StatusCodes.MALFORMED_REQUEST.getErrorCode()) {
                                System.out.println(StatusCodes.MALFORMED_REQUEST.getErrorMsg());
                            } else if (code == StatusCodes.NON_EXISTENT_MEASUREMENT.getErrorCode()) {
                                System.out.println(StatusCodes.NON_EXISTENT_MEASUREMENT.getErrorMsg());
                            }
                        } else {
                            System.out.println("Response did not pass integrity checks.");
                        }
                        break;
                    }
                } catch (SocketTimeoutException s) {
                    if (noOfTries == MAX_RETRIES) {
                        System.out.println("Timeout Exception...Max retries for response exhausted  \n");
                    } else {
                        System.out.println("Request Timeout...Retry " + noOfTries);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                noOfTries++;
                timeoutSecs *= 2;
            }

            noOfTries = 0;
            timeoutSecs = 1;
        }
    }

    private int takeMeasureIdInput() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the Measurement ID for which you want the temperature value: ");
        int measurementId = sc.nextInt();
        if (measurementId > 2 * Short.MAX_VALUE + 1) {
            System.out.println("Measurement ID entered not a 16 bit integer, do you want to convert?(y/n)");
            char choice = sc.next().charAt(0);
            switch (choice) {
                case 'y':
                case 'Y':
                    measurementId = ChecksumManager.get16BitUnsignedInteger(measurementId);
                    break;
                default:
                    System.out.println("Exiting...");
                    System.exit(1);
            }

        }
        return measurementId;
    }

    public void sendRequest(int requestId, int measurementId) throws IOException {
        String requestMsg = CLIENT_MSG_MANAGER.getFormattedRequestMsg(requestId, measurementId);
        byte[] requestMsgBytes = requestMsg.getBytes();
        DatagramPacket requestMsgPacket = new DatagramPacket(requestMsgBytes, requestMsgBytes.length, SERVER_SOCKET_ADDRESS);
        CLIENT_SOCKET_CONNECTION.send(requestMsgPacket);
    }

    private String receiveResponse(byte[] receiveData, int timeout) throws IOException {
        DatagramPacket responsePacket = new DatagramPacket(receiveData, receiveData.length);
        CLIENT_SOCKET_CONNECTION.setSoTimeout(timeout);
        CLIENT_SOCKET_CONNECTION.receive(responsePacket);
        String responseMsg = new String(responsePacket.getData());
        return responseMsg;
    }

    private void destroy() {
        System.out.println("Shutting down client");
        CLIENT_SOCKET_CONNECTION.close();
    }
}
