package com.ents.networks.util;

/**
 * Created by APS on 20-11-2016.
 */
public class ChecksumManager {
    private short getWordFromBytes ( int msb, int lsb) {
        byte byteMsb = (byte)msb;
        byte byteLsb = (byte)lsb;
        short bytesWord = (short) (((msb & 0xFF) << 8) | (lsb & 0xFF));
        return bytesWord;
    }

    public static int get16BitUnsignedInteger(int x) {
        return (x & 0xFFFF);
    }

    public String generateChecksum (String msg) {
        short [] checksumArray = new short[msg.length()];
        int requestMsgLen = msg.length();

        for (int i =0; i< requestMsgLen-1; i++) {
            checksumArray [i] = getWordFromBytes(msg.charAt(i),msg.charAt(i+1));
        }
        if(requestMsgLen%2 !=0){
            checksumArray [requestMsgLen-1] = getWordFromBytes(msg.charAt(requestMsgLen-1),0);
        }

        int S = 0, index = 0,C=7919,D=65536;

        for (int i=0; i < checksumArray.length; i++) {
            index = get16BitUnsignedInteger(S ^ checksumArray [i]);
            S = get16BitUnsignedInteger((C*index)%D);
        }
        return Integer.toString(S);
    }

    public boolean checksumValidate (String msg, String checksum) {
        int calculatedChecksum = Integer.parseInt(generateChecksum(msg));
        int receivedChecksum = Integer.parseInt(checksum);
        return calculatedChecksum == receivedChecksum;
    }
}
